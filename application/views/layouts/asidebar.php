<!-- Brand Logo -->
<a href="../../index3.html" class="brand-link">
  <img src="dist/img/AdminLTELogo.png"
       alt="AdminLTE Logo"
       class="brand-image img-circle elevation-3"
       style="opacity: .8">
  <span class="brand-text font-weight-light">AdminLTE 3</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
  <!-- Sidebar user (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
      <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
      <a href="#" class="d-block"><?php echo ( ! empty($_SESSION['name'])) ? $_SESSION['name'] : ''; ?></a>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->

      <li class="nav-item has-treeview menu-open">
        <a href="#" class="nav-link active">
          <i class="nav-icon fa fa-envelope-o"></i>
          <p>
            Mailbox
            <i class="fa fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="/mailbox/inbox" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Inbox</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/mailbox/compose" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Compose</p>
            </a>
          </li>
          <!--<li class="nav-item">
            <a href="/emailapp/mailbox/read_mail" class="nav-link">
              <i class="fa fa-circle-o nav-icon"></i>
              <p>Read</p>
            </a>
          </li>-->
        </ul>
      </li>
      <li class="nav-item">
        <a href="/user/logout" class="nav-link">
          <i class="nav-icon fa fa-ban"></i>
          <p>
            Logout
          </p>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->