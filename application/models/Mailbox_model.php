<?php
/**
*This file is for database operations
*/

require_once 'application/models/User_model.php';

class Mailbox_model extends User_model {

	/**
	*@name messages
	*@description This function is for retrieve message and sender_name
	*sender_name is mysql function which will return sender name
	*@param $user_id
	*@return $rows, boolean
	*/
	public function messages($user_id)
	{
		try {
			$sql    = "SELECT 
							m.m_id,m.message,m.subject,m.created_date,sender_name(m.fk_sender_id) 
							AS sender_name 
						FROM users AS u 
						INNER JOIN messages AS m 
						ON u.id=m.fk_receiver_id 
						WHERE u.id = $user_id 
						ORDER BY m.m_id DESC";
			
			$result = $this->conn->query($sql);

			if($result->num_rows > 0)
			{
				while($row = $result->fetch_assoc())
				{
					$rows[] = $row;
				}

				return $rows;
			}
			else
			{
				return FALSE;
			}
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	/**
	*@name message_by_id
	*@description This function is for retrieve message by message id, When user want to read the 
	*particular message
	*@param $message_id
	*@return $result, boolean
	*/
	public function message_by_id($message_id)
	{
		try {
			$sql    = "SELECT 
							m.subject,m.message,m.created_date,u.email 
						FROM messages AS m
						INNER JOIN users AS u 
						ON m.fk_sender_id=u.id 
						WHERE m.m_id = $message_id";
			$result = $this->conn->query($sql);

			if($result->num_rows > 0)
			{
				return $result->fetch_assoc();
			}
			else
			{
				return FALSE;
			}
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}
}