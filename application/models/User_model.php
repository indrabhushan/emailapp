<?php
/**
*This file is for database operations
*/

//Including constants file
require_once 'application/config/constants.php';

class User_model {
	
	protected $conn = '';

	/**
	*@description This constructor for creating database connection
	*/
	public function __construct()
	{
		//Create connection
		$this->conn = new mysqli(SERVER_NAME, USER_NAME, PASSWORD, DATABASE);

		if($conn->connect_error)
		{
			die('Connection failed:'. $conn->connect_error);
		}
	}

	/**
	*@name insert
	*@description This function is for inserting data to the database
	*@param $table_name Table name to insert, $data Data associative array to insert
	*@return boolean
	*/
	public function insert($table_name, $data)
	{
		try {

			foreach(array_keys($data) as $key)
			{
				$fields[] = "`$key`";
				$values[] = '"'.$this->conn->real_escape_string($data[$key]).'"';
			} 

			$fields = implode(',', $fields);
			$values = implode(',', $values);
			$sql = " INSERT INTO $table_name ($fields) VALUES ($values)";

			if($this->conn->query($sql))
			{
				$this->conn->commit();
				return TRUE;
			}
			else
			{
				$this->conn->rollback();
				return FALSE;
			}
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	/**
	*@name select
	*@description This function is for the fetching one record from database on certain condition
	*@param $table_name Table name from fetch, $data Column name to fetch, $where Condition
	*@return Fetched associative array, boolean
	*/
	public function select($table_name, $data, $where)
	{
		try {
			$data   = implode(',', $data);
			$field  = array_keys($where);
			$field  = $field[0];
			$sql    = "SELECT $data FROM $table_name WHERE $field = '$where[$field]'";
			$result = $this->conn->query($sql);

			if($result->num_rows > 0)
			{
				return $result->fetch_assoc();
			}
			else
			{
				return FALSE;
			}
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}
}