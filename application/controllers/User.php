<?php
/**
*This file is for user signup and login
*/
session_start();

class User extends Controller {
	
	/**
	*@name register
	*@decription This function is for user registration
	*/
	public function register()
	{
		//If user already logged in
		if( ! empty($_SESSION['user_id']))
		{
			header('location: /mailbox/inbox');
		}

		if(isset($_POST['register']))
		{
			$data = array(
					'full_name'  => $_POST['full_name'],
					'email'      => $_POST['email'],
					'password'   => password_hash($_POST['password'], PASSWORD_BCRYPT),
					'created_by' => $_POST['full_name'],
				);
			$user = $this->model('User_model');

			if($user->insert('users', $data))
			{
				$_SESSION['success_msg'] = 'Registration successfull';
				header('location: /user/login');
			}
			else
			{
				$_SESSION['error_msg'] = 'Registration failed!, Please try again!';
				header('location: /user/register');
			}
		}
		else
		{
			unset($_SESSION);
			$this->view('auth/register');
		}
	}

	/**
	*@name login
	*@description This function is for user login authentication
	*/
	public function login()
	{
		//If user already logged in
		if( ! empty($_SESSION['user_id']))
		{
			header('location: /mailbox/inbox');
		}

		if(isset($_POST['login']))
		{
			$data = array(
					'id',
					'full_name',
					'password',
				);
			$user   = $this->model('User_model');
			$result = $user->select('users', $data, ['email' => $_POST['email']]);

			if($result)
			{
				if(password_verify($_POST['password'], $result['password']))
				{
					$_SESSION = array(
						'user_id' => $result['id'],
						'name'    => $result['full_name'],
						);
					header('location: /mailbox/inbox');
				}
				else
				{
					$_SESSION['error_msg'] = 'Please enter correct password';
					header('location: /user/login');
				}
			}
			else
			{
				$_SESSION['error_msg'] = 'Please enter correct email';
				header('location: /user/login');
			}
		}
		else
		{
			$this->view('auth/login');
		}
	}

	/**
	*@name logout
	*@description This function is for user logout
	*/
	public function logout()
	{
		unset($_SESSION);
		session_destroy();
		header('location: /user/login');
	}
}