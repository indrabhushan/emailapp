<?php
/**
*This file is for listing of all the messages of logged in user, Compose mail and read mail
*/
session_start();

class Mailbox extends Controller {

	public function __construct()
	{
		//If user not logged in
		if(empty($_SESSION['user_id']))
		{
			header('location: /user/login');
		}
	}
	
	/**
	*@name inbox
	*@description This function is showing all the messages of logged in user
	*/
	public function inbox()
	{
		$mailbox  = $this->model('Mailbox_model');
		$messages = $mailbox->messages($_SESSION['user_id']);

		$this->view('mailbox/mailbox', $messages);
	}

	/**
	*@name compose
	*@description This function is for composing the email and sending the email to other 
	*user email id
	*/
	public function compose()
	{
		if(isset($_POST['send']))
		{
			$user   = $this->model('User_model');
			$result = $user->select('users', ['id'], ['email' => $_POST['to_email']]);

			$data = array(
					'subject'     => $_POST['subject'],
					'message'     => $_POST['message'],
					'sender_id'   => $_SESSION['user_id'],
					'receiver_id' => $result['id'],
					'created_by'  => $_SESSION['name'],
				);
			if($user->insert('messages', $data))
			{
				echo '<script>alert("Message send successfully");</script>';
			}
			else
			{
				echo '<script>alert("Message send failed");</script>';	
			}
		}

		$this->view('mailbox/compose');
	}
	
	/**
	*@name read_mail
	*@description This function is for showing description of message when user clicked on message
	*@param $message_id
	*/
	public function read_mail($message_id = '')
	{
		$mailbox = $this->model('Mailbox_model');
		$message = $mailbox->message_by_id($message_id);

		$this->view('mailbox/read-mail', $message);
	}
}