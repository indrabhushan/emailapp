<?php

class App {
	
	protected $controller = 'User';

	protected $method = 'login';

	protected $params = [];

	public function __construct()
	{
		$url = $this->parse_url();

		if(file_exists('application/controllers/'.ucfirst($url[0]).'.php'))
		{
			$this->controller = ucfirst($url[0]);
			unset($url[0]);
		}

		require_once 'application/controllers/'.$this->controller.'.php';

		$this->controller = new $this->controller;

		if(method_exists($this->controller, $url[1]))
		{
			$this->method = $url[1];
			unset($url[1]);
		}

		$this->params = $url ? array_values($url) : [];

		call_user_func_array([$this->controller, $this->method], $this->params);
	}

	public function parse_url()
	{
		if(isset($_GET['url']))
		{
			return explode('/' ,filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
		}
	}
}